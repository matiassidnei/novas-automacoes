#language: pt


  @tms
  Funcionalidade: Tms
  
  @chamadatms
  Esquema do Cenário: TMS
    Dado que acionamos a api do modulo tms
    Quando a mesma acessar com sucesso
    Entao deve retornar o status <status> de sucesso

    Exemplos:
    | status |
    |  200   |
     