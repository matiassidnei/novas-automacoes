require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"

class ClasseTms < ClasseComunicacaoApi
  include ModuloCarregarArquivo

  def tms(token)
    url_api = "#{carrega_arquivo_ambiente['url']['url_base']}/tms"
    construir_busca(token, url_api)
  end
end
