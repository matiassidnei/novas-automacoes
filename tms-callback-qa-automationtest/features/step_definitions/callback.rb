

Dado('que acionamos a api do modulo callback') do
  @token =gerador_token.gerar_token
end

Quando('a mesma acessar com sucesso') do
  @retorno = tms_callback.tmscallback(@token)
end

Entao('deve retornar o status {int} de sucesso') do |status|
expect(@retorno.code).to eql status
end
