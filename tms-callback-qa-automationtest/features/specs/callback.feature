#language: pt


  @tmsCallback
  Funcionalidade: Callback
  
  @chamadatmsCallback
  Esquema do Cenário: TMS Callback
    Dado que acionamos a api do modulo callback
    Quando a mesma acessar com sucesso
    Entao deve retornar o status <status> de sucesso

    Exemplos:
    | status |
    |  200   |
     