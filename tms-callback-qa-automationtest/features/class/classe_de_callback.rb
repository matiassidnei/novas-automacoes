require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"

class ClasseCallback < ClasseComunicacaoApi
  include ModuloCarregarArquivo

  def tmscallback(token)
    url_api = "#{carrega_arquivo_ambiente['url']['url_base']}/tmscallback"
    construir_busca(token, url_api)
  end
end