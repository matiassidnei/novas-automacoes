module ModuloCarregarArquivo
  def carrega_arquivo_ambiente
    YAML.load_file("#{Dir.pwd}/features/support/config/#{ENV['ENVIRONMENT_TYPE']}.yml")
  end

  def carrega_arquivo_massa(nome_arquivo)
    JSON.parse(
        File.read(
          "#{Dir.pwd}/features/support/data/#{nome_arquivo}.json"
        )
      )
  end
end
