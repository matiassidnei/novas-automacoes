module PaginaObjetos

  def comunicador_api
    ClasseComunicacaoApi.new
  end
  
  def gerador_token
    ClasseGerarToken.new
  end

  def tms_callback
    ClasseCallback.new
  end

end
