# frozen_string_literal: true

require 'report_builder'
require 'date'

Before do
  @token = gerador_token.gerar_token
end

at_exit do
  time = Time.now.to_s
  ReportBuilder.configure do |config|
    config.json_path = 'relatorios/report.json'
    config.report_path = 'relatorios/cucumber_relatorio'
    config.report_types = [:html]
    config.report_title = 'Cucumber Report Builder - Testes automatizados em API'
    config.compress_images = false
    config.color = 'indigo'
    config.additional_info = { 'Project name' => 'Team Stocks', 'Platform' => 'RESERVE - API', 'Report generated' => time }
  end
  ReportBuilder.build_report
end
