Dado('que acionamos a api do modulo receive') do
  @token =gerador_token.gerar_token
end

Quando('a mesma acessar com sucesso') do
  @retorno = receive.receive(@token)
end

Entao('deve retornar o status {int} de sucesso') do |status|
  expect(@retorno.code).to eql status
end
