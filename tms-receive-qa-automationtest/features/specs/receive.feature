#language: pt


  @tmsReceive
  Funcionalidade: Receive
  
  @chamadaReceive
  Esquema do Cenário: TMS Receive
    Dado que acionamos a api do modulo receive
    Quando a mesma acessar com sucesso
    Entao deve retornar o status <status> de sucesso

    Exemplos:
    | status |
    |  200   |
     