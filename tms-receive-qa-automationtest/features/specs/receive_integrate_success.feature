#language: pt


  @tmsReceive
  Funcionalidade: Receive_Success
  
  @chamadaReceiveSuccess
  Esquema do Cenário: TMS Receive Success
    Dado que acionamos a api do modulo receive Success
    Quando a mesma enviar o post com sucesso
    Entao deve retornar o status <status> de sucesso

    Exemplos:
    | status |
    |  200   |
     