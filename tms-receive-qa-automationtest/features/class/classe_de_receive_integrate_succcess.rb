require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"

class ClasseReceiveIntegrateSuccess < ClasseComunicacaoApi
  include ModuloCarregarArquivo

  def receive_integrate_success(token)
    url_api = "#{carrega_arquivo_ambiente['url']['url_base']}/tmsreceive?type=success"
    construir_envio(token, massa, url_api)
  end
end