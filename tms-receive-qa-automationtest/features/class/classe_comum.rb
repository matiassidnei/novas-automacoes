class ClasseComum
  def cabecalho_requisicao(token)
    {
      'Authorization' => token.to_s,
      'Content-Type' => 'application/json'
    }
  end

  def construir_envio(token, massa, url_api)
    HTTParty.post(url_api,
                  headers: cabecalho_requisicao(token),
                  body: massa.to_json)
  end

  def construir_busca(token, url_busca)
    HTTParty.get(url_busca,
                 headers: cabecalho_requisicao(token))
  end

  def construir_remocao(token, url_busca)
    HTTParty.delete(url_busca,
                    headers: cabecalho_requisicao(token))
  end

  def construir_alteracao(token, massa, url_api)
    HTTParty.put(url_api,
                 headers: cabecalho_requisicao(token),
                 body: massa.to_json)
  end
end
