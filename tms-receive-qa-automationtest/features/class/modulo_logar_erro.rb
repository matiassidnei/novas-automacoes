module ModuloLogarErro
    def logar_erro_requisicao(retorno)
      sleep 1
      case retorno.code
      when 401..403
        p "Metodo HTTP: #{retorno.request.http_method.to_s.split('::').last.upcase}"
        p "URI: #{retorno.request.uri}"
        p "Codigo de retorno: #{retorno.code}"
        p "Cabecalho da requisicao: #{retorno.request.options[:headers]} "
        p "Corpo da requisicao: #{retorno.request.raw_body}"
        puts "body: #{JSON.pretty_generate(retorno.parsed_response)}"
        exit!
      when 405..408
        p "Metodo HTTP: #{retorno.request.http_method.to_s.split('::').last.upcase}"
        p "URI: #{retorno.request.uri}"
        p "Codigo de retorno: #{retorno.code}"
        p "Cabecalho da requisicao: #{retorno.request.options[:headers]} "
        p "Corpo da requisicao: #{retorno.request.raw_body}"
        puts "body: #{JSON.pretty_generate(retorno.parsed_response)}"
        exit!
      when 410..499
        p "Metodo HTTP: #{retorno.request.http_method.to_s.split('::').last.upcase}"
        p "URI: #{retorno.request.uri}"
        p "Codigo de retorno: #{retorno.code}"
        p "Cabecalho da requisicao: #{retorno.request.options[:headers]} "
        p "Corpo da requisicao: #{retorno.request.raw_body}"
        puts "body: #{JSON.pretty_generate(retorno.parsed_response)}"
        exit!
      when 500..599
        p "Metodo HTTP: #{retorno.request.http_method.to_s.split('::').last.upcase}"
        p "URI: #{retorno.request.uri}"
        p "Codigo de retorno: #{retorno.code}"
        p "#{retorno.code} É PROBLEMA DE AMBIENTE CONTATE O DEVOPS"
        exit!
      end
    end
  end
  