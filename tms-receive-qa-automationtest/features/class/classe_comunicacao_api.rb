class ClasseComunicacaoApi
  def cabecalho_requisicao(token)
    {
      'Authorization' => token.to_s,
      'Content-Type' => 'application/json'
    }
  end

  def construir_envio(token, massa, url_api)
    HTTParty.post(url_api,
                  headers: cabecalho_requisicao(token),
                  body: massa.to_json)
  end

  def construir_busca(token, url_api)
    HTTParty.get(url_api,
                 headers: cabecalho_requisicao(token))
  end

  def construir_atualizacao(token, url_api)
    HTTParty.patch(url_api,
                   headers: cabecalho_requisicao(token))
  end
end
