require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"

class ClasseReceive < ClasseComunicacaoApi
  include ModuloCarregarArquivo

  def receive(token)
    url_api = "#{carrega_arquivo_ambiente['url']['url_base']}/tmsreceive"
    construir_busca(token, url_api)
  end
end