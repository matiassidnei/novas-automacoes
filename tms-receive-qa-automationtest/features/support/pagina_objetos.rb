module PaginaObjetos

  def comunicador_api
    ClasseComunicacaoApi.new
  end
  
  def gerador_token
    ClasseGerarToken.new
  end

  def receive
    ClasseReceive.new
  end
  
  def receive_integrate_success
    ClasseReceiveIntegrateSuccess.new
  end
  
end
