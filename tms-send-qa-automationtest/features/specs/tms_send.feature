#language: pt


  @tms
  Funcionalidade: Tms_Send

  @chamadatmssend
  Esquema do Cenário: TMS_Send
    Dado que acionamos a api do modulo send
    Quando a mesma acessar com sucesso
    Entao deve retornar o status <status> de sucesso

    Exemplos:
    | status |
    |  200   |
     