module PaginaObjetos
  def comunicador_api
    ClasseComunicacaoApi.new
  end

  def gerador_token
    ClasseGerarToken.new
  end

  def tms_send
    ClasseTmsSend.new
  end
end
