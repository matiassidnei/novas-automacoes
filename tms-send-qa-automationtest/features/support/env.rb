require 'cucumber'
require 'httparty'
require 'report_builder'
require 'rspec'
require 'rubocop'
require 'yaml'
require 'pry'
require 'json'
require 'jsonpath'
require 'json-schema'
require 'dbi'
require_relative 'pagina_objetos'
require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"
require_relative "#{Dir.pwd}/features/class/modulo_logar_erro.rb"

World(PaginaObjetos)
World(ModuloCarregarArquivo)
World(ModuloLogarErro)

ENVIRONMENT_TYPE = ENV['ENVIRONMENT_TYPE']
