Dado('que acionamos a api do modulo send') do
  @token = gerador_token.gerar_token
end

Quando('a mesma acessar com sucesso') do
  @retorno = tms_send.tms_send(@token)
end

Entao('deve retornar o status {int} de sucesso') do |status|
  expect(@retorno.code).to eql status
end
