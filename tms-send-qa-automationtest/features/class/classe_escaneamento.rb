require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"

class Escaneamento < ClasseComunicacaoApi
  include ModuloCarregarArquivo

  def busca_escaneamento_id(token, url, id)
    id_busca = MASSA[:escaneamento][:"#{id}"]
    url_api = "#{url}/scans?keyAccess=#{id_busca}"
    realiza_consulta(token, url_api)
  end

  def busca_escaneamento_chave_acesso(token, url, chave_acesso)
    chave_acesso_busca = MASSA[:escaneamento][:"#{chave_acesso}"]
    url_api = "#{url}/scans?keyAccess=#{chave_acesso_busca}"
    realiza_consulta(token, url_api)
  end
end
