require_relative "#{Dir.pwd}/features/class/modulo_carregar_arquivo.rb"

class ClasseTmsSend < ClasseComunicacaoApi
  include ModuloCarregarArquivo

  def tms_send(token)
    url_api = "#{carrega_arquivo_ambiente['url']['url_base']}/tmssend"
    construir_busca(token, url_api)
  end
end
