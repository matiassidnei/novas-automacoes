require_relative 'modulo_carregar_arquivo'
require_relative "#{Dir.pwd}/features/class/modulo_logar_erro.rb"

class ClasseGerarToken
  include ModuloCarregarArquivo
  include ModuloLogarErro

  def gerar_token
    retorno = HTTParty.post(pegar_url,
                            headers: { 'Content-Type' => 'application/json' },
                            body: corpo_requisicao.to_json)
    logar_erro_requisicao(retorno)
    retorno.parsed_response['access_token']
  end

  def pegar_url
    busca_url = carrega_arquivo_ambiente
    busca_url['url']['url_token']
  end

  def corpo_requisicao
    {
      'grant_type': 'client_credentials',
      'client_id': 'app-local',
      'client_secret': 'b2ZmZXItYXBw',
      'scope': 'read'
    }
  end
end
